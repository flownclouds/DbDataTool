package com.mg.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.PathKit;
import com.mg.util.FileUtils;
import com.mg.util.LogRoot;
/**
 * 主controller
 * @author meigang
 * @date 2015-12-19 11:44
 *
 */
@ControllerBind(controllerKey = "/doExcel")
public class ExcelController extends Controller{
	/**
	 * 加载json数据
	 */
	public void loadStudentJson(){
		try {
			String con = FileUtils.loadFileContent(PathKit.getRootClassPath()+"/json/student.json");
			/**
			 * {"page":1,"records":0,"rows":[],"total":0}
			 */
			
			JSONArray arr = JSONArray.parseArray(con);
			int rows = this.getParaToInt("rows");
			int page = this.getParaToInt("page");
			JSONObject res = new JSONObject();
			int records = arr.size();
			int total = 0;
			if(records % rows == 0){
				total = records / rows;
			}else{
				total = records / rows +1;
			}
			res.put("page", page);
			res.put("records", records);
			//对数据分页
			Object sub = arr.subList((page-1)*rows, records - page*rows > 0 ? rows : records);
			res.put("rows", sub);
			LogRoot.info(sub);
			res.put("total", total);
			renderJson(res);
			return ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderJson();
	}
}
