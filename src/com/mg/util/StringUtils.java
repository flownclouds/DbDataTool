package com.mg.util;

public class StringUtils {
	/**
	 * 得到字符串首字母大写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstUpper(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	/**
	 * 首字母小写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstLower(String str) {
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	}

	/**
	 * 全部是大写字母
	 * 
	 * @param word
	 * @return
	 */
	public static boolean isAllUpper(String word) {
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			if (c != '_') {
				if (!Character.isUpperCase(c)) {
					return false;
				}
			}
		}
		return true;
	}
	public static void main(String[] args) {
		String t = "XXX_JJa";
		System.out.println(isAllUpper(t));
	}
	/**
	 * 得到property正确的拼写
	 * @param string
	 * @param px
	 * @return
	 */
	public static String getProperty(String string, int px) {
		/**
		 * px:
		 * 1 - 全大写
		 * 2 - 全小写
		 * 3 - 首字母大写
		 * 4 - 首字母小写
		 */
		String res = string;
		if(px == 1){
			res = string.toUpperCase();
		}else if(px == 2){
			res = string.toLowerCase();
		}else if(px == 3){
			res = firstUpper(string);
		}else if(px == 4){
			res = firstLower(string);
		}
		return res;
	}
	/**
	 * 字符串反转
	 * @param str
	 * @return
	 */
	public static String reverse(String str){
		return new StringBuilder(str).reverse().toString();
	}
}
